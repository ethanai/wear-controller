package com.mobitv.wearcontroller;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;


public class MainActivity extends ActionBarActivity implements MessageApi.MessageListener {
    private final String TAG = getClass().getSimpleName();

    private static final String MEDIA_CONTROL_MESSAGE_PATH = "/mediacontrol";
    private static final String START_ACTIVITY_PATH = "/start-activity";

    private GoogleApiClient mGoogleApiClient;
    private String mConnectedNodeID;
    private static final long CONNECTION_TIME_OUT_MS = 30000;

    GoogleApiClient.ConnectionCallbacks mConnectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(Bundle bundle) {
            Log.d(TAG, "Google API Client was connected");

//        Wearable.DataApi.addListener(mGoogleApiClient, this);
            Wearable.MessageApi.addListener(mGoogleApiClient, MainActivity.this);
//        Wearable.NodeApi.addListener(mGoogleApiClient, this);
        }

        @Override
        public void onConnectionSuspended(int i) {
            Log.d(TAG, "Google API Client was suspended");

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(mConnectionCallbacks)
                .build();

        registerListener();

        sendWearNotification();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
//        Wearable.DataApi.removeListener(mGoogleApiClient, this);
        Wearable.MessageApi.removeListener(mGoogleApiClient, this);
//        Wearable.NodeApi.removeListener(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();

        super.onStop();
    }

    private void startWearableActivity() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Collection<String> nodes = getNodes();
                mConnectedNodeID = nodes.iterator().next();
                sendStartActivityMessage();
            }
        }).start();
    }

    private Collection<String> getNodes() {
        HashSet<String> results = new HashSet<>();
        NodeApi.GetConnectedNodesResult nodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();

        for (Node node : nodes.getNodes()) {
            Log.d(TAG, "Got nodeID: " + node.getId());
            results.add(node.getId());
        }

        return results;
    }

    private void sendStartActivityMessage() {
        if(mConnectedNodeID != null) {
            Log.d(TAG, "sendStartActivityMessage: " + mConnectedNodeID);

            Wearable.MessageApi.sendMessage(mGoogleApiClient, mConnectedNodeID, START_ACTIVITY_PATH, new byte[0])
                    .setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                                           @Override
                                           public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                               if (!sendMessageResult.getStatus().isSuccess()) {
                                                   Log.e(TAG, "Failed to send message with status code: "
                                                           + sendMessageResult.getStatus().getStatusCode());
                                               }
                                           }
                                       }
                    );
        } else {
            Log.d(TAG, "Error: sendStartActivityMessage, mConnectedNodeID is null");
        }
    }

    private void sendWearNotification() {
        int notificationId = 001;

        Intent intent;
        PendingIntent pendingIntent;

        // Build pendingIntent for launching this handheld app from the Wear notification
        intent = new Intent(this, MainActivity.class); // So the wear (or other) notification can re-open the handheld app
        //viewIntent.putExtra(EXTRA_EVENT_ID, eventId);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // Build pendingIntent for the Wear notification button that opens the Wear app
        intent.setData(Uri.parse(MEDIA_CONTROL_MESSAGE_PATH));
        PendingIntent launchWearAppPendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

//        setContentIntent() creates button card within the wearable notification to launch the handheld app
//        There is only one setContentIntent() which is used for the one and only "Open on Phone" command

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.play)
                .setContentTitle("Title Text") // TODO: Put media info here. App name? Album & artist?
                .setContentText("Event Text")
                .setContentIntent(pendingIntent) // For the 'Open on Phone'
                .extend(new NotificationCompat.WearableExtender()
                                .addAction(new NotificationCompat.Action.Builder(R.drawable.play, "Wearable App", launchWearAppPendingIntent).build())
                                .setContentAction(0)
                );

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        // Build the notification and issue it with notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    public void registerListener() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // Get Target Node
                ConnectionResult connectionResult = mGoogleApiClient.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                Log.d(TAG, connectionResult.toString());

                if (mGoogleApiClient.isConnected()) {
                    Wearable.MessageApi.addListener(mGoogleApiClient, MainActivity.this);
                }
            }
        }).start();
    }

    // Allow Wear app to tell this handheld app what commands to send to the dongle
    // Works with MessageApi
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.d(TAG, "Message received.");

        if(messageEvent.getPath().equals(MEDIA_CONTROL_MESSAGE_PATH)) {
            Log.d(TAG, "Message: " + new String(messageEvent.getData()));
            Log.d(TAG, "launchWearApp");
            startWearableActivity();
        }
    }

    // Get command from Wear app via Intent
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getData() == null) {
            Log.d(TAG, "onNewIntent: null data");
            return;
        }
        Log.d(TAG, "onNewIntent: " + intent.getData());
        if (intent.getDataString().equals(MEDIA_CONTROL_MESSAGE_PATH)){
            Log.d(TAG, "launchWearApp");
            startWearableActivity();
        }
    }

    // Button Method
    public void launchWearApp(View view) {
        Log.d(TAG, "launchWearApp");
        startWearableActivity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
