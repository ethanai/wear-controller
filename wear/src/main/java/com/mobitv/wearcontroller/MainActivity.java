package com.mobitv.wearcontroller;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.view.DotsPageIndicator;
import android.support.wearable.view.GridViewPager;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.Set;
import java.util.concurrent.TimeUnit;

public class MainActivity extends Activity {
    private final String TAG = getClass().getSimpleName();

    private static final String MEDIA_CONTROL_CAPABILITY = "media_control_capability";
    private static final String HUE_CONTROL_CAPABILITY = "hue_control_capability";

    private static final String MEDIA_CONTROL_MESSAGE_PATH = "/mediacontrol";
    private String mTargetNodeID;

    private GoogleApiClient mGoogleApiClient;
    private static final long CONNECTION_TIME_OUT_MS = 30000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                GridViewPager gridViewPager = (GridViewPager) findViewById(R.id.gridViewPager);
                gridViewPager.setAdapter(new ControllerPagerAdapter(getBaseContext()));
                // Note: don't use getApplicationContext() for the context. It will break the ability
                // for buttons to use methods specified by XML.

                // Set up page indicator
                DotsPageIndicator dotsPageIndicator = (DotsPageIndicator) findViewById(R.id.pageIndicator);
                dotsPageIndicator.setPager(gridViewPager);
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();

        sendMessage("onCreate");


    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//
//        mGoogleApiClient.disconnect();
//    }

    // Media Control Functions
    public void volumeUp(View view) {
        sendMessage("volumeUp");
    }

    public void volumeDown(View view) {
        sendMessage("volumeDown");
    }

    public void seekBack(View view) {
        sendMessage("seekBack");
    }

    public void seekForward(View view) {
        sendMessage("seekForward");
    }

    public void togglePlayPause(View view) {
        sendMessage("togglePlayPause");
    }

    // Hue Control Functions
    public void hueLightsOn(View view) {
        sendMessage("hueLightsOn");
    }

    public void hueLightsDim(View view) {
        sendMessage("hueLightsDim");
    }

    public void hueLightsOff(View view) {
        sendMessage("hueLightsOff");
    }

    public void sendMessage(final String messageString) {
        Log.d(TAG, "Send Message: " + messageString);
        new Thread(new Runnable() {
            @Override
            public void run() {
                // Get Target Node
                ConnectionResult connectionResult = mGoogleApiClient.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                Log.d(TAG, connectionResult.toString());

                if (mGoogleApiClient.isConnected()) {
                    CapabilityApi.GetCapabilityResult result = Wearable.CapabilityApi.getCapability(
                            mGoogleApiClient, MEDIA_CONTROL_CAPABILITY, CapabilityApi.FILTER_REACHABLE)
                            .await();
                    CapabilityInfo capabilityInfo = result.getCapability();
                    Set<Node> connectedNodes = capabilityInfo.getNodes();
                    Log.d(TAG, "Connected Capable Nodes: " + connectedNodes.size());
                    if(connectedNodes.size() > 0) {
                        Node node = connectedNodes.iterator().next();
                        mTargetNodeID = node.getId();
                        Log.d(TAG, "Connected To: " + node.getDisplayName());
                    }



//                    CapabilityApi.GetAllCapabilitiesResult allResult = Wearable.CapabilityApi.getAllCapabilities(mGoogleApiClient, CapabilityApi.FILTER_ALL).await();
//                    Map allCapabilityInfo = allResult.getAllCapabilities();
//
//                    Log.d(TAG, "All Connected Nodes: " + allCapabilityInfo.size());
//
//                    NodeApi.GetConnectedNodesResult result3 = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
//                    Log.d(TAG, "Connected Nodes: " + result3.getNodes().size());
//
//                    List<Node> nodeList = result3.getNodes();
//
//                    for(Node node : nodeList) {
//                        Log.d(TAG, node.getDisplayName() + " nearby " + node.isNearby());
//                    }

                    // Send the message
                    byte[] messageBytes = messageString.getBytes();

                    if (mTargetNodeID == null) {
                        Log.d(TAG, "Error: No target");
                    }

                    PendingResult<MessageApi.SendMessageResult> messageResult = Wearable.MessageApi.sendMessage(mGoogleApiClient, mTargetNodeID, MEDIA_CONTROL_MESSAGE_PATH, messageBytes);
                    MessageApi.SendMessageResult messageResult2 = messageResult.await();

                    Log.d(TAG, "Message sent: " + messageBytes + " status: " + messageResult2.getStatus());
                } else {
                    Log.d(TAG, "Error, not connected.");
                }
            }
        }).start();
    }


}
