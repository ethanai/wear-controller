package com.mobitv.wearcontroller;

import android.content.Context;
import android.support.wearable.view.GridPagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by esmith on 5/8/15.
 */
public class ControllerPagerAdapter extends GridPagerAdapter {
    final String TAG = getClass().getSimpleName();

    final Context mContext;

    public ControllerPagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    public int getColumnCount(int i) {
        return 2;
    }

    @Override
    protected Object instantiateItem(ViewGroup viewGroup, int row, int col) {
        Log.d(TAG, "instantiateItem: row=" + row + " col=" + col);
        View view;
        switch(col) {
            case 0:
                view = View.inflate(mContext, R.layout.rect_activity_media, null);
                break;
            case 1:
                view = View.inflate(mContext, R.layout.rect_activity_hue, null);
                break;
            default:
                view = View.inflate(mContext, R.layout.rect_activity_hue, null);
        }
        viewGroup.addView(view);
        return view;
    }

    @Override
    protected void destroyItem(ViewGroup container, int row, int col, Object view) {
        container.removeView((View)view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }
}
